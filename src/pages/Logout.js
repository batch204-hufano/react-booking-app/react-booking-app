import {useContext, useEffect, useInsertionEffect } from 'react';
import { Navigate } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Logout(){

    //get the setUser setter from App.js
    const { setUser } = useContext(UserContext);

    //clear localStorage
    localStorage.clear();

    //when the component mounts/page loads, set the user 
    useEffect(()=> {
        setUser({
            email: null
        })
    })

    return(
        <Navigate to="/login" />
    )
}