// import courseData from "../data/courseData.js"
import CourseCard from '../components/CourseCard.js'
import { useEffect, useState } from "react"

export default function Courses () {

    const[ coursesData, setCoursesData ] = useState([])

    // Check to see if the mock data wa captured
    // console.log(courseData) ;
    // console.log(courseData[0]);


    useEffect(() => {

        // console.log(process.env.REACT_APP_API_URL)
        // changes to env files are applied Only at build time (when)

        fetch(`${process.env.REACT_APP_API_URL}/courses`)
        .then(res => res.json())
        .then(data => {
            console.log(data)
            setCoursesData(data)
        })
    },[])

    const courses = coursesData.map(course => {
        return(
            <CourseCard courseProp = {course} key={course._id}/>
        )
    })
    return (
        <>
            <h1>Courses</h1>
            {courses}
        </>
    )
}