import Banner from '../components/Banner.js'
import Highlights from '../components/Highlights.js'


export default function Home() {

    const data = {
        title: "Zuitt Coding Bootcamp",
        content: "Oppurtunities for everyone, everywhere.",
        destination: "/courses",
        label: "Enroll now!"
    }

    return (
        <>
            <Banner dataProp= {data} />
            <Highlights />
        </>
    )
}