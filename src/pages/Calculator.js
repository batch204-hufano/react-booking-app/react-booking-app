// import { Form,Button,Row,Col } from 'react-bootstrap';
// import { useState,useEffect } from 'react'

// export default function Calculator() {
    
//     const [number1,setNumber1] = useState("")
//     const [number2,setNumber2] = useState("")
//     const [operator,setOperator] = useState("")
//     const [result,setResult]= useState("")

//     const calc = () => {
//         if(number1 !== '' && number2 !== ''){
//             if(operator === "+"){
//             return parseFloat(number1)+parseFloat(number2)
//             } else if (operator === "-"){
//                 return number1-number2
//             } else if (operator === "*"){
//                 return number1*number2
//             } else if (operator === "/") {
//                 return number1/number2
//             }
//         } else {
//             return 0
//         }
//     }

//     const clearInput = () => {
//         setNumber1("")
//         setNumber2("")
//         setOperator("")
//     }

//     useEffect(()=> {
//         setResult(calc())
//     }, [number1,number2,operator])

//     return (
//         <Row>
//             <Col xs={12} className="p-5 text-center">
//                 <h1>Calculator</h1>
//                 <h2>{result}</h2>
//             </Col>
//             <Col xs={6}>
//                 <Form>
//                     <Form.Group>
//                         <Form.Label></Form.Label>
//                         <Form.Control 
//                             type="number"
//                             placeholder="Input number"
//                             value={number1}
//                             onChange= {e=> setNumber1(e.target.value)}
//                             required
//                         />
//                         <Form.Text>
//                         </Form.Text>
//                     </Form.Group>
//                 </Form>
//             </Col>
//             <Col xs={6}>
//                 <Form>
//                     <Form.Group>
//                         <Form.Label></Form.Label>
//                         <Form.Control 
//                             type="number"
//                             placeholder="Input number"
//                             value={number2}
//                             onChange= {e=> setNumber2(e.target.value)}
//                             required
//                         />
//                         <Form.Text>
//                         </Form.Text>
//                     </Form.Group>
//                 </Form>
//             </Col>
//             <Col xs={12} className="pt-5 text-center">
//                 <Button className="mx-2" value={"+"} onClick={e => setOperator(e.target.value)}>Add</Button>
//                 <Button className="mx-2" value={"-"} onClick={e => setOperator(e.target.value)}>Subtract</Button>
//                 <Button className="mx-2" value={"*"} onClick={e => setOperator(e.target.value)}>Multiply</Button>
//                 <Button className="mx-2" value={"/"} onClick={e => setOperator(e.target.value)}>Divide</Button>
//                 <Button className="mx-2" onClick={clearInput}>Clear</Button>
//             </Col>
//         </Row>
//     )
// }