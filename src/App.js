import { useState } from 'react';

import {Container} from 'react-bootstrap'
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'; //the as keyword gives an alias to the component upon import

import './App.css';
import AppNavBar from './components/AppNavBar.js';
import Courses from './pages/Course';
import Error from './pages/Error';
import Home from './pages/Home.js'
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';


import { UserProvider } from './UserContext'
// import Calculator from './pages/Calculator';
/*
  All other components/pages will be contained in our main component: <App />
*/
function App() {

  const [user, setUser] = useState({
    email: localStorage.getItem("email")
  })

  return (
    // The UserProvider component has a value attribute that we can use to pass our user state to our components
    <UserProvider value={{user, setUser}}>
      <Router>
        <AppNavBar />
        <Container >
          <Routes>
            <Route exact path="/" element= {<Home/>} />
            <Route exact path="/courses" element= {<Courses/>} />
            <Route exact path="/register" element= {<Register/>} />
            <Route exact path="/login" element= {<Login/>} />
            <Route exact path="/logout" element= {<Logout/>} />
            {/* <Route exact path="/calculator" element= {<Calculator/>} /> */}
            <Route exact path="*" element= {<Error/>} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  );
}

export default App;
